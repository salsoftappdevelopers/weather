
import React, { Fragment, useEffect, useState } from 'react';
import { StyleSheet, Button, Text, View, ImageBackground, Image,  PermissionsAndroid, Platform} from 'react-native';

import { FlatList,TouchableOpacity } from 'react-native-gesture-handler';
import BottomSheet1 from 'reanimated-bottom-sheet';
import weather from '../../api/weatherApi';
import Geolocation from '@react-native-community/geolocation';
import moment from 'moment';
import size from '../screens/ScreenSize/getWidthHeight';
import Spinner from 'react-native-loading-spinner-overlay';

const BottomSheet = ({ navigation, route }) => {


  const [result, setResult] = useState(null);
  const [current, setCurrent] = useState(null);
  const [hourTemp, setHourtemp] = useState(null);
  const [dayTemp, setdayTemp] = useState(null);
  const [loading, setLoading] = useState(true);
  const [loc, setLoc] = useState(null);
  const [locationaccess, setLocationaccess] = useState(false);
  const [temperature, setTemperature] = useState(0);

  const [
    currentLongitude,
    setCurrentLongitude
  ] = useState("");
  const [
    currentLatitude,
    setCurrentLatitude
  ] = useState("");

  var location;

  // const [city,setCity]=useState({city : "Gilgit",country: "Pakistan"});


  // console.log('city',city.city);
  var today;
  let city;

  var array = [];
  // let results= {};
  //  const [searchapi,astronomy_get] = useResults();
  //  searchapi("Karachi");
  //  const results=astronomy_get("Karachi");
  // console.log("new results",result);
  // console.log("astro results",astroResult);

  const searchapi = async (searchterm) => {

    //console.log('hi there');

    if (route !== undefined) {
      searchterm = route.params.city;
      //  setCity(route.params.city);
      //  setCountry(route.params.country);
      setLoc({ tz_id: route.params.city },
        { country: route.params.country });
    }

    try {
      const response = await weather.get('/current.json', {
        params: {
          q: searchterm,
          key: 'f978a66825464b638f7140522200910'
        }
      });
      // console.log("response",response.data);

      // console.log("city",city);
      // console.log("country",loc);

      setCurrent(response.data.current);
      setLoc(response.data.location);

      console.log("current results", current);

    }
    catch (err) {
      console.log("error", err.message);
      //setErrormessage("something went wrong");
    }

  };

  const astronomy_get = async (searchterm) => {


    console.log('hi there');
    console.log('current city', searchterm);


    // if(route!==undefined)
    // {
    //   searchterm=route.params.city;
    //   console.log("change");
    //   // setCity(route.params.city);
    //   // setCountry(route.params.country);
    // }


    try {
      const response = await weather.get('/astronomy.json', {
        params: {
          q: searchterm,
          key: 'f978a66825464b638f7140522200910'
        }
      });
      // console.log("astronomy response",response.data);
      //hour_temp("Karachi");
      // setResult(response.data.businesses);
      //sunrise= response.data.astronomy.astro.sunrise;
      //setAstronomy(response.data.astronomy);

      // results=response.data.astronomy;
      setResult(response.data.astronomy);
      setLoading(false);

      // searchapi("Karachi");
      // return results;
      //console.log('s',sunrise);
    }
    catch (err) {

      // console.log("error",err.message);
      //setErrormessage("something went wrong");
    }

  };

  const getTracks = () => {
    console.log("there");
    if (route === undefined) {
      console.log("back", current);
      Geolocation.getCurrentPosition(position => {

        setLocationaccess(true)
        const currentLongitude =
          JSON.stringify(position.coords.longitude);


        //getting the Latitude from the location json
        const currentLatitude =
          JSON.stringify(position.coords.latitude);

        let current_location = position.coords.latitude + "," + position.coords.longitude;

        // setCurrentLongitude(currentLongitude);

        // // //     //Setting Longitude state
        // setCurrentLatitude(currentLatitude); 



        // console.log("position", position);


        // console.log("location",current_location);

        astronomy_get(current_location);
        searchapi(current_location);
        hour_temp(current_location, 3);
        setLoading(true);

      }, (error) => {
        console.log("loc error", error);
        setLoading(false)
        setLocationaccess(false)
      },
        {
          enableHighAccuracy: true,
          timeout: 5000

        },
      );
    }
    else {
      astronomy_get(route.params.city);
      searchapi(route.params.city);
      hour_temp(route.params.city, 3);
    }
  };
  // const check_params=()=>{
  //   if(route!==undefined)
  //   {
  //   console.log("params",route);
  //   setCity({city: route.params.city,country:route.params.country});
  //   }
  // };
  const hour_temp = async (searchterm, days) => {


    // console.log('hi');
    if (route !== undefined) {
      // console.log('ss');
      searchterm = route.params.city;
      // setCity(route.params.city);
      // setCountry(route.params.country);
    }

    try {
      const response = await weather.get('/forecast.json', {
        params: {
          q: searchterm,
          key: 'f978a66825464b638f7140522200910',
          days: days
        }
      });
      // console.log("response hour",response.data);

      // setResult(response.data.businesses);
      //sunrise= response.data.astronomy.astro.sunrise;
      //setAstronomy(response.data.astronomy);

      results = response.data.astronomy;
      today = moment().utcOffset('+05').format('hh a');
      //var today=moment().format('YYYY-MM-DD hh:mm:ss a');
      //  console.log("time",today);

      //hourTemp=response.data.forecast.forecastday[0].hour




      setHourtemp(response.data.forecast.forecastday[0].hour);
      setdayTemp(response.data.forecast);
      // array=hourTemp.slice(today,hourTemp.length);
      // console.log("array",array);
      // console.log("api results",hourTemp);

      // searchapi("Karachi");
      // return results;
      //console.log('s',sunrise);
    }
    catch (err) {
      console.log("error", err.message);
      //setErrormessage("something went wrong");
    }

  };

  const requestLocationPermission = async () => {

    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {

        }
      );

      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("permission access");
      }
      else {
        console.log("permission not access");

      }


    } catch (err) {

      console.log("location error", err);

    }
  };
  const Farhenheit_temp=()=>{
      return(
        
        <ImageBackground source={require("../../assets/image/background_image_weather.png")}
          style={{
            flex: 1,
            height: undefined, width: undefined
          }}>

          <View style={{ justifyContent: "center", marginTop: size.vh * 2 }}>
            <TouchableOpacity style={styles.touchable_style} onPress={() => {
              navigation.navigate("countries"

              )
            }}>
              <View style={{ flexDirection: "row", justifyContent: "center" }}>
                <Text style={{ color: "white", fontSize: size.vh * 2, fontFamily: "Dosis-Regular" }}>{loc && loc.name},</Text>

                <Text style={{ color: "#d3d3d3", fontSize: size.vh * 2, fontFamily: "Dosis-Regular" }}>{loc && loc.country}</Text>
              </View>
            </TouchableOpacity>
          </View>

          <Text style={{ color: "white", textAlign: "center", fontSize: size.vw * 10, marginTop: size.vh * 5.2, fontFamily: "Dosis-Regular" }}>Today</Text>
          <Text style={{ color: "#d3d3d3", textAlign: "center", fontSize: size.vh * 1.8, marginTop: size.vh * 0.2, fontFamily: "Dosis-Regular" }}>{current_date}</Text>
           
         
       
        { temperature===0 ?     <TouchableOpacity onPress={ ()=> 
              {
                  setTemperature(1)
              }}>
              
              <View style={{flexDirection:'row',justifyContent:"center"}}>
              <Text style={{ color: "white", textAlign: "center", fontSize: size.vh * 7, fontFamily: "Dosis-Regular" }}>{current && current.temp_c}</Text>
              <Text style={{ color: "white", textAlign: "center", fontSize: size.vh * 3, fontFamily: "Dosis-Regular" }}>°</Text>
              <Text style={{ color: "white", textAlign: "center", fontSize: size.vh * 3,  fontFamily: "Dosis-Regular" }}>C</Text>
              </View>

              </TouchableOpacity>
         
           : <TouchableOpacity onPress={ ()=> 
            { 
              alert('message')
                setTemperature(0)
            }}>
            
            <View style={{flexDirection:'row',justifyContent:"center"}}>
            <Text style={{ color: "white", textAlign: "center", fontSize: size.vh * 7, fontFamily: "Dosis-Regular" }}>{current && current.temp_f}</Text>
            <Text style={{ color: "white", textAlign: "center", fontSize: size.vh * 3, fontFamily: "Dosis-Regular" }}>°</Text>
            <Text style={{ color: "white", textAlign: "center", fontSize: size.vh * 3,  fontFamily: "Dosis-Regular" }}>C</Text>
            </View>

            </TouchableOpacity>
       
        }
         
          <Text style={{ color: "#d3d3d3", textAlign: "center", fontSize: size.vh * 1.8, marginTop: size.vh * 0.2, marginHorizontal: size.vw * 2, fontFamily: "Dosis-Regular" }}>feels like {current && current.feelslike_c}°</Text>

          <View style={styles.today_rowstyle}>
            <Text style={{ color: "white", marginLeft: 25, marginTop: size.vh * 3, fontSize: size.vh * 2, fontFamily: "Dosis-Regular" }}>Today</Text>
            <TouchableOpacity style={styles.touchable_style} onPress={() => {
              navigation.navigate("weather", {
                forecastDays: dayTemp
              })
            }}>
              <Text style={{ color: "white", marginRight: 25, marginTop: size.vh * 2, fontSize: size.vh * 2, fontFamily: "Dosis-Regular" }}>Next 3 days </Text>
            </TouchableOpacity>

          </View>


          <View
            style={{
              borderBottomColor: '#c0c0c0',
              borderBottomWidth: 0.4,
              marginTop: size.vh * 2,
              marginBottom: 10

            }}
          />
          <FlatList

            horizontal={true}
            data={array}
            renderItem={({ item }) => {



              var array1 = item.time.split(" ");
              var hours = array1[1].split(':');

              // gives the value in 24 hours format
              var AmOrPm = hours[0] >= 12 ? 'pm' : 'am';
              var cur_hour = (hours[0] % 12) || 12;
              var finalTime = cur_hour + " " + AmOrPm;
              //  console.log("array",array);

             
                return <View>

                  <Text style={{ color: "white", marginLeft: 20, fontSize: size.vh * 2, marginTop: size.vh * 0.1, fontFamily: "Dosis-Regular" }}>{finalTime}</Text>
                  <View style={styles.flatlist_style}>
                    <Image style={{ height: size.vh * 5, width: size.vw * 7 }} source={{ uri: `https:${item.condition.icon}` }} />
                    <View >
                      <Text style={styles.flatlist_Textstyle}>{item.temp_c}°</Text>
                    </View>

                  </View>

                </View>
              
            }
            }>

          </FlatList>

        </ImageBackground>

        // <BottomSheet1
        //   ref={sheetRef}
        //   snapPoints={[size.vh * 2, size.vh * 35, 40]}
        //   borderRadius={20}
        //   initialSnap={1}
        //   renderHeader={() => <View style={styles.header} />}
        //   enabledContentGestureInteraction={true}
        //   renderContent={renderContent}


        // />
        
      )    
    

  }

  const renderContent = () => (


    <View
      style={{
        backgroundColor: 'white',
        paddingBottom: size.vh * 5,
        height: size.vh * 50,
      }}
    >

      <View style={styles.rowStyle}>
        <Text style={styles.headerStyle}>SUNRISE</Text>
        <Text style={styles.headerStyle}>SUNSET</Text>
      </View>

      <View style={styles.rowStyle}>
        <Text style={styles.headerListStyle}>{result && result.astro.sunrise}</Text>
        <Text style={styles.headerListStyle}>{result && result.astro.sunset}</Text>
      </View>

      <View
        style={{
          borderBottomColor: '#f5f5f5',
          borderBottomWidth: 3,
          marginTop: size.vh * 2,


        }}
      />
      <View style={styles.rowStyle}>
        <Text style={styles.headerStyle}>PRECIPITATION</Text>
        <Text style={styles.headerStyle}>HUMIDITY</Text>
      </View>

      <View style={styles.rowStyle}>

        <Text style={styles.headerListStyle}>{current && current.precip_in} %</Text>
        <Text style={styles.headerListStyle}>{current && current.humidity} %</Text>
      </View>


      <View
        style={{
          borderBottomColor: '#f5f5f5',
          borderBottomWidth: 2,
          marginTop: 20,


        }}
      />

      <View style={styles.rowStyle}>
        <Text style={styles.headerStyle}>WIND</Text>
        <Text style={styles.headerStyle}>PRESSURE</Text>
      </View>

      <View style={styles.rowStyle}>

        <Text style={styles.headerListStyle}>{current && current.wind_kph} km/h</Text>
        <Text style={styles.headerListStyle}>{current && current.pressure_in} hPa</Text>
      </View>

    </View>
  );

  const sheetRef = React.useRef(null);

  useEffect(() => {
    console.log("log");
    if (Platform.OS === 'android') {
      requestLocationPermission();
    }
    getTracks();
    //check_params();


  }, []);

  // console.log("results",result);
  // console.log("ht",hourTemp);
  today = moment().utcOffset('+05').format('HH');
  // console.log("today",today);

  {
    hourTemp !== null ? array = hourTemp.slice(today, hourTemp.length)
    : null
  }

  var current_date = moment().format("ddd, D MMM");
  // if(route!==undefined)
  // {
  // console.log("params",route);

  // }
  if (locationaccess===true || loading===true )   {
    return (


      <>
        <Spinner
          visible={loading}

        />


        <ImageBackground source={require("../../assets/image/background_image_weather.png")}
          style={{
            flex: 1,
            height: undefined, width: undefined
          }}>

          <View style={{ justifyContent: "center", marginTop: size.vh * 2 }}>
            <TouchableOpacity style={styles.touchable_style} onPress={() => {
              navigation.navigate("countries"

              )
            }}>
              <View style={{ flexDirection: "row", justifyContent: "center" }}>
                <Text style={{ color: "white", fontSize: size.vh * 2, fontFamily: "Dosis-Regular" }}>{loc && loc.name},</Text>

                <Text style={{ color: "#d3d3d3", fontSize: size.vh * 2, fontFamily: "Dosis-Regular" }}>{loc && loc.country}</Text>
              </View>
            </TouchableOpacity>
          </View>

          <Text style={{ color: "white", textAlign: "center", fontSize: size.vw * 10, marginTop: size.vh * 5.2, fontFamily: "Dosis-Regular" }}>Today</Text>
          <Text style={{ color: "#d3d3d3", textAlign: "center", fontSize: size.vh * 1.8, marginTop: size.vh * 0.2, fontFamily: "Dosis-Regular" }}>{current_date}</Text>
          
          {
            
            temperature===0 ?<TouchableOpacity onPress={ ()=> 
          {
              setTemperature(1)
          }}>
          
          <View style={{flexDirection:'row',justifyContent:"center"}}>
           <Text style={{ color: "white", textAlign: "center", fontSize: size.vh * 7, fontFamily: "Dosis-Regular" }}>{current && current.temp_c}</Text>
           <Text style={{ color: "white", textAlign: "center", fontSize: size.vh * 3, fontFamily: "Dosis-Regular" }}>°</Text>
           <Text style={{ color: "white", textAlign: "center", fontSize: size.vh * 3,  fontFamily: "Dosis-Regular" }}>C</Text>
           </View>

          </TouchableOpacity>
          
          :<TouchableOpacity onPress={ ()=> 
            {
                setTemperature(0)
            }}>
            
            <View style={{flexDirection:'row',justifyContent:"center"}}>
             <Text style={{ color: "white", textAlign: "center", fontSize: size.vh * 7, fontFamily: "Dosis-Regular" }}>{current && current.temp_f}</Text>
             <Text style={{ color: "white", textAlign: "center", fontSize: size.vh * 3, fontFamily: "Dosis-Regular" }}>°</Text>
             <Text style={{ color: "white", textAlign: "center", fontSize: size.vh * 3,  fontFamily: "Dosis-Regular" }}>F</Text>
             </View>
  
            </TouchableOpacity>
        }
        {
         temperature===0 ? <Text style={{ color: "#d3d3d3", textAlign: "center", fontSize: size.vh * 1.8, marginTop: size.vh * 0.2, marginHorizontal: size.vw * 2, fontFamily: "Dosis-Regular" }}>feels like {current && current.feelslike_c}°</Text>
                        :  <Text style={{ color: "#d3d3d3", textAlign: "center", fontSize: size.vh * 1.8, marginTop: size.vh * 0.2, marginHorizontal: size.vw * 2, fontFamily: "Dosis-Regular" }}>feels like {current && current.feelslike_f}°</Text>
        
        }
          <View style={styles.today_rowstyle}>
            <Text style={{ color: "white", marginLeft: 25, marginTop: size.vh * 3, fontSize: size.vh * 2, fontFamily: "Dosis-Regular" }}>Today</Text>
            <TouchableOpacity style={styles.touchable_style} onPress={() => {
              navigation.navigate("weather", {
                forecastDays: dayTemp
              })
            }}>
              <Text style={{ color: "white", marginRight: 25, marginTop: size.vh * 2, fontSize: size.vh * 2, fontFamily: "Dosis-Regular" }}>Next 3 days </Text>
            </TouchableOpacity>

          </View>


          <View
            style={{
              borderBottomColor: '#c0c0c0',
              borderBottomWidth: 0.4,
              marginTop: size.vh * 2,
              marginBottom: 10

            }}
          />
          <FlatList

            horizontal={true}
            data={array}
            renderItem={({ item }) => {



              var array1 = item.time.split(" ");
              var hours = array1[1].split(':');

              // gives the value in 24 hours format
              var AmOrPm = hours[0] >= 12 ? 'pm' : 'am';
              var cur_hour = (hours[0] % 12) || 12;
              var finalTime = cur_hour + " " + AmOrPm;
              //  console.log("array",array);

                if(temperature===0)
                {
                return <View>

                  <Text style={{ color: "white", marginLeft: 20, fontSize: size.vh * 2, marginTop: size.vh * 0.1, fontFamily: "Dosis-Regular" }}>{finalTime}</Text>
                  <View style={styles.flatlist_style}>
                    <Image style={{ height: size.vh * 5, width: size.vw * 7 }} source={{ uri: `https:${item.condition.icon}` }} />
                    <View >
                      <Text style={styles.flatlist_Textstyle}>{item.temp_c}°</Text>
                    </View>

                  </View>

                </View>
                }
                else{
                  
                  return <View>

                  <Text style={{ color: "white", marginLeft: 20, fontSize: size.vh * 2, marginTop: size.vh * 0.1, fontFamily: "Dosis-Regular" }}>{finalTime}</Text>
                  <View style={styles.flatlist_style}>
                    <Image style={{ height: size.vh * 5, width: size.vw * 7 }} source={{ uri: `https:${item.condition.icon}` }} />
                    <View >
                      <Text style={styles.flatlist_Textstyle}>{item.temp_f}°</Text>
                    </View>

                  </View>

                </View>

                }
              
            }
            }>

          </FlatList>

        </ImageBackground>

        <BottomSheet1
          ref={sheetRef}
          snapPoints={[size.vh * 2, size.vh * 35, 40]}
          borderRadius={20}
          initialSnap={1}
          renderHeader={() => <View style={styles.header} />}
          enabledContentGestureInteraction={true}
          renderContent={renderContent}


        />



      </>
    );
  }

  else {

    return (


      <>
        {/* <Spinner
          visible={loading}

        /> */}


        <ImageBackground source={require("../../assets/image/background_image_weather.png")}
          style={{
            flex: 1,
            height: undefined, width: undefined,
            justifyContent: "center"
          }}>



          <Text style={{ color: "white", textAlign: "center", fontSize: size.vw * 10, marginTop: size.vh * 5.2, fontFamily: "Dosis-Regular" }}>Today</Text>
          <Text style={{ color: "#d3d3d3", textAlign: "center", fontSize: size.vh * 1.8, marginTop: size.vh * 0.2, fontFamily: "Dosis-Regular" }}>{current_date}</Text>




          <View
            style={{
              borderBottomColor: '#c0c0c0',
              borderBottomWidth: 0.4,
              marginTop: size.vh * 2,
              marginBottom: 10

            }}
          />

          <TouchableOpacity
         
          onPress={() => {
            if(Platform.OS==='ios')
            {
              Geolocation.requestAuthorization();
            }

            requestLocationPermission();
            
            getTracks()
          }
          }>

            <View style={styles.Button_style}
            >

              <Text style={styles.permission_style}>Grant for permission</Text>

            </View>


          </TouchableOpacity>

        </ImageBackground>


      </>
    );
  }

};

const styles = StyleSheet.create({
  rowStyle: {
    flexDirection: "row",
    marginTop: 10,
    justifyContent: 'space-around'
  },
  today_rowstyle: {
    flexDirection: "row",
    marginTop: size.vh * 3,
    justifyContent: "space-between"
  },
  headerStyle: {
    color: 'gray',
    fontWeight: 'bold',
    fontSize: size.vh * 2,
    fontFamily: "Dosis-Regular"
  },
  flatlist_style: {
    backgroundColor: '#FFFFFF40',
    opacity: 2,
    marginTop: size.vh * 0.5,
    width: size.vw * 13,
    height: size.vh * 12,
    borderRadius: size.vh * 3.5,
    marginLeft: 10,
    justifyContent: "center",
    alignItems: "center"


  },
  headerListStyle: {
    fontSize: size.vw * 4,
    color: 'dimgray',
    fontWeight: "bold",
    fontFamily: "Dosis-Regular"

  },
  flatlist_Textstyle: {
    fontSize: size.vh * 2,
    color: "white",
    //marginHorizontal:size.vh*1,
    fontWeight: "bold",
    fontFamily: "Dosis-Regular"
  },

  touchable_style: {
    color: "blue"


  },
  header: {
    backgroundColor: 'white',
    width: 80,
    height: size.vh * 0.4,
    margin: 5,
    alignSelf: "center",
    borderRadius: 11,
  },
  Button_style: {
    borderRadius: 25,
    backgroundColor: 'white',
    justifyContent: "center",
    alignSelf: "center",
    marginVertical: 20,
    width: 150,
    height: 50
  },
  permission_style: {
    color: 'purple',
    fontSize: 15,
    alignSelf: "center",
    justifyContent: "center",
    fontFamily: "Dosis-Regular"
  },
  view_style: {
    width: 250,
    height: 150,
    backgroundColor: 'white',
    borderRadius: 15,
  },
  permissiontext_style: {
    color: 'black',
    fontSize: 18,
    alignSelf: "center",
    fontFamily: "Dosis-Regular"
  },
});

export default BottomSheet;