import React,{useEffect,useState} from 'react';
import { StyleSheet, Text, View, Button, ImageBackground,Image,TouchableOpacity } from 'react-native';
 import { FlatList } from 'react-native-gesture-handler';
import Animated from 'react-native-reanimated';
import BottomSheet1 from 'reanimated-bottom-sheet';
import weather from '../../api/weatherApi';
import moment from 'moment';
import Accordion from 'react-native-collapsible/Accordion';
import { Separator } from 'native-base';
import {Collapse,CollapseHeader, CollapseBody, AccordionList} from 'accordion-collapse-react-native';
import { Component } from 'react';
import size from './ScreenSize/getWidthHeight';



  var list;
  var i=0;
  const state={
    list
  }
  
  class weather_nextdays extends Component {
    
   
   
      
      _head(item){
          console.log("item",item);
          var dt = new Date( item.date );
          return(
              <Separator bordered style={{alignItems:"stretch",  height:size.vh*10}}>

                  <View style={style.rowStyle}>
                      <View style={style.rowStyle}>
                      <Image style={{height:size.vh*6,width:size.vw*6}}  source={{uri : `https:${item.day.condition.icon}`}}/>
                  <Text style={{textAlign:"center",marginVertical:10,fontSize:size.vh*2,marginLeft:15,fontFamily:"Dosis-Regular"}}>{dt.toDateString()}</Text>
                      </View>
                  
                  <View style={style.rowStyle}>
                  <Text style={{marginVertical:5,fontSize:size.vh*2,marginRight:10,fontFamily:"Dosis-Regular"}}>{item.day.maxtemp_c}°</Text>
                  <Text style={{marginVertical:5,fontSize:size.vh*2,marginRight:20,color:'gray',fontFamily:"Dosis-Regular"}}>{item.day.mintemp_c}°</Text>

                  </View>
                  
                  </View>
                
              </Separator>
          );
      }
      
      _body(item){
          return (
              <View>
                     <View style={style.content_style}>
            <Text style={style.headerStyle}>Precipitation</Text>
          <Text style={{color:'gray',fontSize:size.vh*2,fontFamily:"Dosis-Regular"}}>{item.day.totalprecip_mm} %</Text>
            <Text style={style.headerStyle}>Humidity</Text>
            <Text style={{color:'gray',fontSize:size.vh*2,fontFamily:"Dosis-Regular"}}>{item.day.avghumidity} %</Text>
         </View>

         <View style={style.content_style}>
            <Text style={style.headerStyle}>wind</Text>
          <Text style={{color:'gray',fontSize:size.vh*2,fontFamily:"Dosis-Regular"}}>{item.day.maxwind_kph} km/h</Text>
            <Text style={style.headerStyle}>pressure</Text>
            <Text style={{color:'gray',fontSize:size.vh*2,fontFamily:"Dosis-Regular"}}>{item.day.avgvis_km} hPa</Text>
         </View>
              </View>
           
          );
      }
      
   
    
    render() {

        console.log("props",this.props.route.params);
        list=this.props.route.params.forecastDays.forecastday;
        console.log("props",list);
      return (

         <View>

         <View style={{flexDirection:"row"}}>

          <TouchableOpacity   onPress={()=> {this.props.navigation.goBack()}}>
           <Image source={require('../../assets/image/back.png')}
                 style={{height:size.vh*4,width:size.vh*4,marginVertical:35,marginHorizontal:10,alignSelf:"flex-start"}}
               /> 
        
           </TouchableOpacity>
             <Text style={style.textStyle}>Next 3 Days</Text>

             </View>
              <AccordionList
            list={list}
            header={this._head}
            body={this._body}
            keyExtractor={item => (item)}
          />
         </View>
       
      );
    }
  }
const style=StyleSheet.create({
    textStyle:{
        fontSize:20,
        marginLeft:20,
        marginTop:30,
        marginBottom:20,
        fontFamily: "Dosis-Regular",
        fontSize:size.vh*4
       
    },
    rowStyle:{
        flexDirection:"row",
        justifyContent:"space-between"
        
    
      },
      flatlist_Textstyle:{
        fontSize:12,
       
        marginHorizontal:10,
        fontFamily: "Dosis-Regular"
      },
      headerStyle:{
       fontSize:size.vh*2
       
      },
      content_style:{
        flexDirection:"row", 
        justifyContent:"space-around",
        paddingTop:20,
        paddingBottom:15
        
      }
  
});

export default weather_nextdays;