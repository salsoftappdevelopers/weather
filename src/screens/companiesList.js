import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, Button, ImageBackground, Image, TouchableOpacity } from 'react-native';
import { FlatList, TextInput } from 'react-native-gesture-handler';
import Animated from 'react-native-reanimated';
import BottomSheet1 from 'reanimated-bottom-sheet';
import weather from '../../api/weatherApi';
import moment from 'moment';
import Accordion from 'react-native-collapsible/Accordion';
import { Form, Separator } from 'native-base';
import { Collapse, CollapseHeader, CollapseBody, AccordionList } from 'accordion-collapse-react-native';
import { Component } from 'react';
import countries from '../../countries_name/country';
import { SearchBar } from 'react-native-elements';
import size from '../screens/ScreenSize/getWidthHeight';
import Spinner from 'react-native-loading-spinner-overlay';


var check ;
var i = 0;
var current_item;
var value;


class companiesList extends Component {

  state = {
    search: '',
    city:''
  };
  constructor(props) {
    super(props);
  }

  updateSearch = (search) => {
    this.setState({  search });
  };

  updateCity = (city) => {
     console.log("length",value);
    this.setState({  city: city });
    // for(let i=0;i<countries[item].length;i++)
    // {
    //   if(countries[item][i].includes(this.state.city))
    //   {
    //     list.push(countries[item]);
    //     console.log("list item",list);
    //   }
    // }
  };
   updateLocation=()=>{
    this.setState({ keys });
  };



  _head = (item) => {

    value = countries[item];
    
   
    if (item.includes(this.state.search))
    {
     
       return (
    <View>
         

             
      <Separator bordered style={{ alignItems: "stretch", height: size.vh*9 }}>
     
        <Text style={{ marginVertical: 5, marginRight: 10,fontSize:size.vh*2 ,fontFamily:"Dosis-Regular"}}>{item}</Text>


      </Separator>
        </View>
    );
    }


  }

  chechCity=(item)=>{
    console.log("expand",item);
         
  }
 

  _body = (item) => {
    
   
    
    return (
      <View >

        <View style={{flexDirection:"row",backgroundColor:'#778899'}}>

        
       <View style={{backgroundColor:'white',alignSelf:"stretch",flex:1,borderRadius:20,marginVertical:size.vh,marginHorizontal:size.vh*1.5}}>
        <TextInput
        placeholder="Search"
        inputStyle={{fontSize:size.vh*2.5}}
        onChangeText={this.updateCity}
        value={this.state.city}
        style={{flex:1,alignSelf:"stretch",marginLeft:15}}
        // containerStyle={{flex:1,marginVertical:size.vh*2}}
        ></TextInput>
        </View> 

        </View>
        
         
       
        
         <FlatList
          keyExtractor={(item, index) => String(index)}
          initialNumToRender={5}
          maxToRenderPerBatch={10}
          data={countries[item]}
          
          renderItem={(value) => {
          
            current_item=value.item;
          
          //  console.log("value",value);
           if(value.item.includes(this.state.city))
           {
           console.log(value.item);
          
            return <View>
              <TouchableOpacity  onPress={()=> this.props.navigation.navigate("Bottom",{
                           city:value.item,
                           country: item
                       })} >
                <Text style={{marginLeft:size.vh*3,padding:size.vh*2,color:'grey',fontFamily:"Dosis-Regular",fontSize:size.vh*2}}>{value.item}</Text>
             
              </TouchableOpacity>


            </View>
           }
          //  else{
          //  this.setState({check: false})
          //  }

          
          }
          }>

        </FlatList>
      
  

       {
        //  console.log("item",current_item)
        // (!current_item.includes(this.state.city))  ?  <Text style={{marginLeft:size.vh*3,padding:size.vh*2,color:'grey',fontFamily:"Dosis-Regular",fontSize:size.vh*2}}>No city found</Text>
                                               
        //                               :  null
      } 
                                          
      </View>

    );
  }



  render() {

    // console.log("props",this.props.route.params);

    
    const { search } = this.state;
    // console.log("size",size);
    var keys = Object.keys(countries);
    // console.log("props",this.props);

    return (

      <View>

     

         <View style={{flexDirection:"row",backgroundColor:'#778899'}}>

           <TouchableOpacity   onPress={()=> {this.props.navigation.goBack();
                                                 this.setState({search:''})}}>
           <Image source={require('../../assets/image/icon_white.png')}
                 style={{height:size.vh*3,width:size.vh*4,marginVertical:20,marginHorizontal:5,alignSelf:"flex-start"}}
               /> 
        
           </TouchableOpacity>
          
          <View style={{backgroundColor:'white',alignSelf:"stretch",flex:1,borderRadius:20,marginVertical:10,marginEnd:10}}>
          <TextInput
        placeholder="Search"
        inputStyle={{fontSize:size.vh*2}}
        onChangeText={this.updateSearch}
        value={search}
        style={{flex:1,alignSelf:"stretch",marginLeft:15}}
        // containerStyle={{flex:1,marginVertical:size.vh*2}}
      ></TextInput>
          </View>
          
         </View>
          
        <AccordionList
          
          list={keys}
          header={this._head}
          body={this._body}
          keyExtractor={item => (item)}
         
          
        />
      </View>

    );
  }
};



const style = StyleSheet.create({
  textStyle: {
    fontSize: 20,
    marginLeft: 20,
    marginTop: 30,
    marginBottom: 20

  },
  rowStyle: {
    flexDirection: "row",
    justifyContent: "space-between"


  },
  flatlist_Textstyle: {
    fontSize: 12,

    marginHorizontal: 10
  },
  headerStyle: {
    fontSize: 12

  },
  content_style: {
    flexDirection: "row",
    justifyContent: "space-around",
    paddingTop: 20,
    paddingBottom: 15

  }

});

export default companiesList;