import {Dimensions } from "react-native";

var screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

var size={
    vw: screenWidth/100,
    vh: screenHeight/100
}



export default size;