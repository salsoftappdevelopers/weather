/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Button,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import { NavigationContainer, StackActions } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from './src/screens/HomeScreen';
import weather_nextdays from './src/screens/weather_nextdays';
import companiesList from './src/screens/companiesList';
import BottomSheet from './src/screens/BottomSheet';
const Stack = createStackNavigator();

const App= ()=> {
  return (
    <NavigationContainer>
      <Stack.Navigator >
        <Stack.Screen name="Home" component={HomeScreen} 
                      options={{
                        headerShown: false
                    }}></Stack.Screen>

         <Stack.Screen name="weather" component={weather_nextdays} 
                      options={{
                        headerShown: false
                    }}></Stack.Screen>
                    
                    <Stack.Screen name="countries" component={companiesList} 
                      options={{
                        headerShown: false,
                        headerLeft: (
                          <Button
                            onPress={() => alert('This is a button!')}
                            title="Info"
                            color="#fff"
                          />
                        ),
                    }}></Stack.Screen>

<Stack.Screen name="Bottom" component={BottomSheet} 
                      options={{
                        headerShown: false
                    }}></Stack.Screen>



      </Stack.Navigator>
      
    </NavigationContainer>
  );
};

const styles = StyleSheet.create({
 
  
});

export default App;
