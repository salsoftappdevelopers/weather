import axios from 'axios';

export default axios.create({
    baseURL:'https://api.weatherapi.com/v1',

    headers: {
        Authorization:
          'Bearer f978a66825464b638f7140522200910'
      }
});
